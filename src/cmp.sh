OPS="-I. -g -ggdb -O0 `mysql_config --cflags` `pkg-config --libs --cflags glib-2.0` `pkg-config --libs gthread-2.0` -fPIC -DMYSQL_DYNAMIC_PLUGIN"
#echo $OPS
gcc -c fundadb.c $OPS -DDEFINE_VARIABLES
gcc -c hash.c $OPS -DDESTROY_ARRAY
gcc -c queue.c $OPS
gcc fundadb.o hash.o queue.o -o fundadb.so $OPS -shared
