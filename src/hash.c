#include "fundadb.h"

//#define WITH_ARRAY


my_bool fdb_get_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
	CHECK_HASH_INIT
    if (args->arg_count != 1)
    {
        strcpy(message, "fdb_get() can only accept one argument");
        return 1;
    }
    if (args->arg_type[0] != STRING_RESULT)
    {
        strcpy(message, "fdb_get() argument has to be a string");
        return 1;
    }
    return 0;
}


char *fdb_get(UDF_INIT *initid, UDF_ARGS *args, char *result,
    unsigned long *length, char *is_null, char *error)
{
    initid->ptr=NULL;
    char *value=NULL;
	int ret=0;
    unsigned char i=(unsigned char) (args->args[0][0]);
    if((args->lengths[0])>2) i=i+args->args[0][1]+args->args[0][2];
    i=i%num_hashes;
    pthread_rwlock_rdlock(&fdb_hashes[i]->lock);
    fdb_hash_entry *entry=g_hash_table_lookup(fdb_hashes[i]->hash, args->args[0]);
    if (entry!=NULL) {
		time_t t=time(NULL);
		if (entry->expire > t) { 
	        if (entry->length>UDF_BUFFER_SIZE) {
				value=g_malloc(entry->length);
    			initid->ptr=value;
    	    	memcpy(value,entry->value,entry->length);
				ret=UDF_BUFFER_NO;
			} else {
    			//initid->ptr=NULL;
    	    	memcpy(result,entry->value,entry->length);
				ret=UDF_BUFFER_YES;
			}
        	*length=entry->length;
		}
    }
    __sync_fetch_and_add(&fdb_hashes[i]->cntGet,1);
    pthread_rwlock_unlock(&fdb_hashes[i]->lock);
//    if (value==NULL) {
    if (ret==0) {
        *is_null=1;
        return NULL;
	}
    if (ret==UDF_BUFFER_NO) return value;
    if (ret==UDF_BUFFER_YES) return result;
}



void *fdb_get_deinit(UDF_INIT *initid)
{
    if (initid->ptr) g_free(initid->ptr);
}


my_bool fdb_set_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
    CHECK_HASH_INIT
    if (args->arg_count > 3 || args->arg_count < 2)
    {
        strcpy(message, "fdb_set() can only accept 2 or 3 arguments ");
        return 1;
    }
    if (args->arg_type[0] != STRING_RESULT || args->arg_type[1] != STRING_RESULT)
    {
        strcpy(message, "the first two arguments of fdb_set() have to be a string");
        return 1;
    }

/*    if (args->arg_count==3)
        if (args->arg_type[2] != INT_RESULT)
    {
        strcpy(message, "the third argument of cdb_set() has to be an integer");
        return 1;
    } */
    return 0;
}

long long fdb_set(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error)
{
    fdb_hash_entry *entry = g_malloc(sizeof(fdb_hash_entry));
    entry->key=g_strndup(args->args[0], args->lengths[0]);
    entry->value=g_strndup(args->args[1], args->lengths[1]);
    entry->length=args->lengths[1];
    entry->self=entry;
	if (args->arg_count==3) {
		long long tt;
		if ( args->arg_type[2] == INT_RESULT) {
			tt=*(long long *)args->args[2];
		} else if ( args->arg_type[2] == STRING_RESULT) {
			tt=atol(args->args[2]);
		} else {
			tt=fdb_system_var.hash_expire_default;
		} 
		if ( tt > fdb_system_var.hash_expire_max ) {
			entry->expire=tt;
		} else {
			entry->expire=time(NULL)+tt;
		}
	} else {
		entry->expire=time(NULL)+fdb_system_var.hash_expire_default;
    }
	unsigned char i=(unsigned char) (args->args[0][0]);
    if((args->lengths[0])>2) i=i+args->args[0][1]+args->args[0][2];
    i=i%num_hashes;
    pthread_rwlock_wrlock(&fdb_hashes[i]->lock);
#ifdef DESTROY_ARRAY
//	entry->hash=cdb_hashes[i];
    g_ptr_array_add(fdb_hashes[i]->ptrArray, entry);
#endif /* DESTROY_ARRAY */
    // instead of REPLACE, push in queue
    g_hash_table_replace(fdb_hashes[i]->hash, entry->key, entry);
    __sync_fetch_and_add(&fdb_hashes[i]->cntSet,1);
    pthread_rwlock_unlock(&fdb_hashes[i]->lock);
    return 0;
}

my_bool fdb_del_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
    CHECK_HASH_INIT
    if (args->arg_count != 1 || args->arg_type[0] != STRING_RESULT)
    {
        strcpy(message, "fdb_del() can only accept one string argument");
        return 1;
    }
    return 0;
}

long long fdb_del(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error)
{
    /// MODIFY to push in the queue
    long long ret;
    unsigned char i=(unsigned char) (args->args[0][0]);
    if((args->lengths[0])>2) i=i+args->args[0][1]+args->args[0][2];
    i=i%num_hashes;
    pthread_rwlock_wrlock(&fdb_hashes[i]->lock);
    ret=g_hash_table_remove (fdb_hashes[i]->hash,args->args[0]);
    __sync_fetch_and_add(&fdb_hashes[i]->cntDel,1);
    pthread_rwlock_unlock(&fdb_hashes[i]->lock);
    return ret;
}

void hash_value_destroy_func(void * hash_entry) {
    fdb_hash_entry *entry= (fdb_hash_entry *) hash_entry;
#ifdef DESTROY_ARRAY
    entry->expire=EXPIRE_DROPIT;
//	g_ptr_array_add(entry->hash->ptrArray, hash_entry);
#else
    g_free(entry->key);
    g_free(entry->value);
    g_free(entry->self);
#endif /* DESTROY_ARRAY */
}


/*
void fdb_hashes_destroy() {
	unsigned char i;
    fprintf(stderr, "CacheDB: destroying Hash Tables:");
    for (i=0; i<num_hashes; i++) pthread_rwlock_wrlock(&fdb_hashes[i]->lock);
    for (i=0; i<num_hashes; i++) g_hash_table_destroy(fdb_hashes[i]->hash);
    //for (i=0; i<num_hashes; i++) cdb_hashes[i]->hash = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, hash_value_destroy_func);
	for (i=0; i<num_hashes; i++) g_ptr_array_free(fdb_hashes[i]->ptrArray, TRUE);
    for (i=0; i<num_hashes; i++) pthread_rwlock_unlock(&fdb_hashes[i]->lock);
	for (i=0; i<num_hashes; i++) free(fdb_hashes[i]);
	free(fdb_hashes);
	fprintf(stderr, " done!\n");
}
*/


// I still need to ensure that no more than 1 fdb_hashes_new can run at the same time, need to add a global lock
void fdb_hashes_new() {
    unsigned char i;
    fprintf(stderr, "FundaDB: initializing Hash Tables:");
    fdb_hashes=calloc(sizeof(fdb_hash_t *),num_hashes);
    for (i=0; i<num_hashes; i++) {
        fdb_hashes[i]=malloc(sizeof(fdb_hash_t));
        fdb_hashes[i]->hash = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, hash_value_destroy_func);
        fdb_hashes[i]->cntDel = 0;
        fdb_hashes[i]->cntGet = 0;
        fdb_hashes[i]->cntSet = 0;
        fdb_hashes[i]->purgeIdx = 0;
        pthread_rwlock_init(&(fdb_hashes[i]->lock), NULL);
		fdb_hashes[i]->ptrArray=g_ptr_array_new();
    }
    fdb_system_var.hash_purge_time=HASH_PURGETIME_USEC;
    fdb_system_var.hash_purge_loop=HASH_PURGELOOP_USEC;
    fdb_system_var.hash_expire_max=HASH_EXPIRE_MAX;
    fdb_system_var.hash_expire_default=HASH_EXPIRE_DEFAULT;

    __sync_bool_compare_and_swap(&hash_initialized,0,1);
	fprintf(stderr, " done!\n");
}



void *purgeHash_thread(void *arg) {
	unsigned long long min_idx;
	while(1) {
		usleep(fdb_system_var.hash_purge_loop);
		//if ((__sync_fetch_and_add(&initialized, 0))==0) continue;
		unsigned char i;
		for (i=0; i<num_hashes; i++) {
			pthread_rwlock_wrlock(&fdb_hashes[i]->lock);
			if (fdb_hashes[i]->purgeIdx==0) {
				if (fdb_hashes[i]->ptrArray->len) {
					fdb_hashes[i]->purgeIdx=fdb_hashes[i]->ptrArray->len;
					fdb_hashes[i]->purgeChunkSize=fdb_hashes[i]->purgeIdx*fdb_system_var.hash_purge_loop/fdb_system_var.hash_purge_time;
				}
			}
			time_t t=time(NULL);
			min_idx=( fdb_hashes[i]->purgeIdx - fdb_hashes[i]->purgeChunkSize > 0 ? fdb_hashes[i]->purgeIdx - fdb_hashes[i]->purgeChunkSize : 0 ) ;
#ifdef DESTROY_ARRAY
			if (fdb_hashes[i]->purgeIdx) while( --fdb_hashes[i]->purgeIdx > min_idx) {
				fdb_hash_entry *entry=g_ptr_array_index(fdb_hashes[i]->ptrArray,fdb_hashes[i]->purgeIdx);
				if (( entry->expire!=EXPIRE_DROPIT) && entry->expire <= t) {
					g_hash_table_remove (fdb_hashes[i]->hash,entry->key);
				}
				if (entry->expire==EXPIRE_DROPIT) {
					g_free(entry->key);
					g_free(entry->value);
					g_free(entry->self);
					g_ptr_array_remove_index_fast(fdb_hashes[i]->ptrArray,fdb_hashes[i]->purgeIdx);
				}
			}
#endif  /* DESTROY_ARRAY */

			pthread_rwlock_unlock(&fdb_hashes[i]->lock);
		}
	}
}


my_bool fdb_hash_init_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
    //CHECK_HASH_INIT
    if (hash_initialized==1)
    {
        strcpy(message, "Hash table already initialized");
        return 1;
    }
    if (args->arg_count != 0)
    {
        strcpy(message, "fdb_hash_init() does not accept argument");
        return 1;
    }
    return 0;
}




long long fdb_hash_init(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error)
{
    fdb_hashes_new();
    pthread_t thr;
    pthread_create(&thr, NULL, purgeHash_thread, NULL);
    return 0;
}
