#include <stdlib.h>
#include <string.h>
#include <mysql.h>
#include <stdio.h>
#include <mysql/plugin.h>
#include <mysql_version.h>
#include <glib.h>
#include <pthread.h>
#include <unistd.h>
#include <time.h>
#include <assert.h>

#ifdef DEFINE_VARIABLES
#define EXTERN
#else
#define EXTERN extern
#endif /* DEFINE_VARIABLES */

#define num_hashes 24
#define EXPIRE_DROPIT   0

#define HASH_PURGETIME_USEC  10000000
#define HASH_PURGELOOP_USEC  100000
#define HASH_EXPIRE_MAX              365*24*3600
#define HASH_EXPIRE_DEFAULT  3600
#define UDF_BUFFER_SIZE 255
#define UDF_BUFFER_YES  1
#define UDF_BUFFER_NO   2


// the follow macro is used to verify if the connections were initialized
#define CHECK_HASH_INIT if ((__sync_fetch_and_add(&hash_initialized, 0))==0) { strcpy(message, "No avaliable servers"); return 1; }
#define CHECK_QUEUE_INIT if ((__sync_fetch_and_add(&queue_initialized, 0))==0) { strcpy(message, "No avaliable servers"); return 1; }


#ifdef DEFINE_VARIABLES
unsigned int hash_initialized=0;
unsigned int queue_initialized=0;
#else
extern unsigned int hash_initialized;
extern unsigned int queue_initialized;
#endif /* DEFINE_VARIABLES */

void fdb_hashes_new();
my_bool fdb_set_init(UDF_INIT *, UDF_ARGS *, char *);
long long fdb_set(UDF_INIT *, UDF_ARGS *, char *, char *);

typedef struct __fdb_queue_msg_t {
    gpointer data;
    unsigned long length;
} fdb_queue_msg_t;

typedef struct __fdb_queue_t {
    pthread_rwlock_t lock;
    unsigned long refcnt;
    unsigned long length;
    gchar *name;
    GAsyncQueue *queue;
} fdb_queue_t;

typedef struct __fdb_queue_superblock_t {
    pthread_rwlock_t lock;
    GTree *queue_names;
    unsigned long long size;
} fdb_queue_superblock_t;

typedef struct __fdb_hash_t {
    pthread_rwlock_t lock;
    GHashTable *hash;
    GPtrArray *ptrArray;
    unsigned long long dataSize;
    unsigned long long purgeChunkSize;
    unsigned long long purgeIdx;
    unsigned long long cntDel;
    unsigned long long cntGet;
    unsigned long long cntSet;
} fdb_hash_t;

typedef struct __fdb_hash_entry {
    char *key;
    char *value;
    fdb_hash_t *hash;
    struct __fdb_hash_entry *self;
    unsigned long klen;
    unsigned long length;
    time_t expire;
    time_t access;
} fdb_hash_entry;

typedef struct __fdb_system_var_t {
    long long hash_purge_time;
    long long hash_purge_loop;
    long long hash_expire_max;
    long long hash_expire_default;
} fdb_system_var_t;


EXTERN fdb_system_var_t fdb_system_var;
EXTERN fdb_hash_t **fdb_hashes;
EXTERN fdb_queue_superblock_t *fdb_queues;
