#include "fundadb.h"



gpointer fdb_queue_name_lookup(gconstpointer key, int refcnt) {
    fdb_queue_t *ret;
    pthread_rwlock_rdlock(&fdb_queues->lock);
    ret=g_tree_lookup(fdb_queues->queue_names,key);
    if (ret && refcnt) {
      __sync_fetch_and_add(&ret->refcnt,1);
    }
    pthread_rwlock_unlock(&fdb_queues->lock);
    return ret;
}

my_bool fdb_queue_create_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
    CHECK_QUEUE_INIT
    if (args->arg_count != 1 || args->arg_type[0] != STRING_RESULT)
    {
        strcpy(message, "fdb_queue_create() can only accept one string argument");
        return 1;
    }
    return 0;
}


long long fdb_queue_create(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error) {
    fdb_queue_t *q;
    long long ret=1;
    pthread_rwlock_wrlock(&fdb_queues->lock);
    q=g_tree_lookup(fdb_queues->queue_names,args->args[0]);
    if (q==NULL) {
        q=malloc(sizeof(fdb_queue_t));
        assert(q);
        pthread_rwlock_init(&(q->lock), NULL);
        q->refcnt=0;
        q->length=0;
        q->name=g_strndup(args->args[0],args->lengths[0]);
        q->queue=g_async_queue_new();
        g_tree_insert(fdb_queues->queue_names,q->name,q);
        fdb_queues->size++;
        ret=0;
    }
    pthread_rwlock_unlock(&fdb_queues->lock);
    return ret;
}

my_bool fdb_queue_push_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
    CHECK_QUEUE_INIT
    if (args->arg_count != 2)
    {
        strcpy(message, "fdb_queue_push() can only accept 2 arguments");
        return 1;
    }
    if (args->arg_type[0] != STRING_RESULT || args->arg_type[1] != STRING_RESULT)
    {
        strcpy(message, "the two arguments of fdb_queue_push() must be strings");
        return 1;
    }
   return 0; 
}

long long fdb_queue_push(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error)
{
    //return 0;   // smoke test
    fdb_queue_t *q;
    q=fdb_queue_name_lookup(args->args[0],1);
    if (q==NULL) {  // queue not found
        return 1;
    }
    fdb_queue_msg_t *msg=g_slice_alloc(sizeof(fdb_queue_msg_t));
    msg->data = g_strndup(args->args[1],args->lengths[1]);
    if (msg->data==NULL) { // g_strndup failed
        __sync_fetch_and_sub(&q->refcnt,1);
        return 1;
    }
    msg->length=args->lengths[1];
    g_async_queue_push(q->queue,msg);
    __sync_fetch_and_sub(&q->refcnt,1);
    return 0;
}


my_bool fdb_queue_pop_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
    CHECK_QUEUE_INIT
    if (args->arg_count > 2 || args->arg_count < 1)
    {
        strcpy(message, "fdb_queue_pop() can only accept one or two arguments");
        return 1;
    }
    if (args->arg_type[0] != STRING_RESULT)
    {
        strcpy(message, "fdb_queue_pop() argument has to be a string");
        return 1;
    }
    return 0;
}


char *fdb_queue_pop(UDF_INIT *initid, UDF_ARGS *args, char *result,
    unsigned long *length, char *is_null, char *error)
{
    initid->ptr=NULL;
    //return NULL; // smoke test
    char *value=NULL;
    int ret=0;
    fdb_queue_t *q;
    fdb_queue_msg_t *msg=NULL;
	/* timeout */
	long long tt=0;
	if (args->arg_count==2) {
		if ( args->arg_type[1] == INT_RESULT) {
			tt=*(long long *)args->args[1];
		} else if ( args->arg_type[1] == STRING_RESULT) {
			tt=atol(args->args[1]);
		}
	}
    q=fdb_queue_name_lookup(args->args[0],1);
    if (q==NULL) {  // queue not found
        *is_null=1;
        return NULL;
    }
	if (args->arg_count==1) /* no timeout */ {
    	msg=g_async_queue_pop(q->queue);
	} else { /* timeout */
    	msg=g_async_queue_timeout_pop(q->queue,tt);
		if (msg==NULL) return NULL;
	}	
    *length=msg->length;
    initid->ptr=(char *)msg;
    __sync_fetch_and_sub(&q->refcnt,1);
    return msg->data;
    //if (ret==UDF_BUFFER_NO) return value;
    //if (ret==UDF_BUFFER_YES) return result;
}

void *fdb_queue_pop_deinit(UDF_INIT *initid)
{
    if (initid->ptr) {
        fdb_queue_msg_t *msg=(fdb_queue_msg_t *)initid->ptr;
        g_free(msg->data);
        g_slice_free1(sizeof(fdb_queue_msg_t),msg);
    }
}

void fdb_queue_new() {
    fprintf(stderr, "FundaDB: initializing Queue Tables:");
    fdb_queues=malloc(sizeof(fdb_queue_superblock_t));
    assert(fdb_queues);
    pthread_rwlock_init(&(fdb_queues->lock), NULL);
    fdb_queues->queue_names=g_tree_new((GCompareFunc)g_strcmp0);
    assert(fdb_queues->queue_names);
    fdb_queues->size=0;
    __sync_bool_compare_and_swap(&queue_initialized,0,1);
    fprintf(stderr, " done!\n");
}


my_bool fdb_queue_init_init(UDF_INIT *initid, UDF_ARGS *args, char *message)
{
    if (queue_initialized==1)
    {
        strcpy(message, "Queue table already initialized");
        return 1;
    }
    if (args->arg_count != 0)
    {
        strcpy(message, "fdb_queue_init() does not accept argument");
        return 1;
    }
    return 0;
}




long long fdb_queue_init(UDF_INIT *initid, UDF_ARGS *args, char *is_null, char *error)
{
	g_thread_init(NULL);
	fdb_queue_new();
    return 0;
}
