=======
FundaDB
=======

FundaDB is a set of UDF functions that adds capability to MySQL

It currently implements:

* a key/value storage

* asynchronous queues

The key/value storage tries to solve two common issues:

* avoid the use of a connection to the database plus a connection to a key/value storage, integrating the latter inside mysqld

* easy to use integration and merge of data between key/value and mysqld, with migration of data in both direction

The asynchronous queueing system create an efficient way for workers to communicate. Any application able to send queries to mysql can become a producer or a consumer in a system where communication happens in real time.

Functions
=========

* **fdb_hash_init** : initializes the key/value storage
* **fdb_set** : stores a key/value
* **fdb_get** : gets a value

* **fdb_queue_init** : initializes the queueing system
* **fdb_queue_create** : creates a named queue
* **fdb_queue_push** : pushs a value into a named queue
* **fdb_queue_pop** : pops a value from a named queue


Installation
============

Please read *BUGS & Known Issues* before proceeding

Download & compile
~~~~~~~~~~~~~~~~~~
Download::

  git clone git@bitbucket.org:renecannao/fundadb.git
  cd src
  sh cmp.sh

TODO #1: add a list of pre-requisites

TODO #2: add configure and Makefile

Important: glib 2.32 is an important pre-requisite, don't run with older version



Install the functions
~~~~~~~~~~~~~~~~~~~~~

Copy shared library::

  shell> cp fundadb.so /path/to/mysql/plugin/directory

Run the follow into mysql command line to create the functions::

  CREATE FUNCTION fdb_hash_init RETURNS INTEGER SONAME 'fundadb.so';
  CREATE FUNCTION fdb_set RETURNS INTEGER SONAME 'fundadb.so';
  CREATE FUNCTION fdb_get RETURNS STRING SONAME 'fundadb.so';
  CREATE FUNCTION fdb_queue_init RETURNS INTEGER SONAME 'fundadb.so';
  CREATE FUNCTION fdb_queue_create RETURNS INTEGER SONAME 'fundadb.so';
  CREATE FUNCTION fdb_queue_push RETURNS INTEGER SONAME 'fundadb.so';
  CREATE FUNCTION fdb_queue_pop RETURNS STRING SONAME 'fundadb.so';


Initialize the system
~~~~~~~~~~~~~~~~~~~~~

To use the key/value storage and the queueing system, they need to be initialized calling::

	mysql> SELECT fdb_hash_init();
	mysql> SELECT fdb_queue_init();

It is recommended to add these two functions into an SQL file to be called via **init_file** .


Key/Value storage
=================

Syntax
~~~~~~

* fdb_hash_init()

  Initialize the key/value storage

  Returns:

  - 0 : success
  - error if system already initialized


* fdb_hash_set( *'key'*, *'value'* [ , *timeout* ] )

  It creates a new key/value entry. If the entry already exists it is overwritten.
  It is possible to specify a timeout in second.

  *Note:* :

  - Currently there is no upper limit in the number of entries that can be stored.

  - keys are *key sensitive*

* fdb_hash_get ( *'key'* )

  Returns:

  - the value if the key is present and not expires
  - NULL if the key doesn't exist or is expired




Examples of simple usage
~~~~~~~~~~~~~~~~~~~~~~~~

Example::

  mysql> select fdb_set('key1','value1');
  +--------------------------+
  | fdb_set('key1','value1') |
  +--------------------------+
  |                        0 |
  +--------------------------+
  1 row in set (0.00 sec)
  
  mysql> select fdb_get('key1');
  +-----------------+
  | fdb_get('key1') |
  +-----------------+
  | value1          |
  +-----------------+
  1 row in set (0.00 sec)
  
  mysql> select fdb_get('key2');
  +-----------------+
  | fdb_get('key2') |
  +-----------------+
  | NULL            |
  +-----------------+
  1 row in set (0.00 sec)
  
  mysql> select fdb_set('key2','value2',2);
  +----------------------------+
  | fdb_set('key2','value2',2) |
  +----------------------------+
  |                          0 |
  +----------------------------+
  1 row in set (0.00 sec)
  
  mysql> select fdb_get('key2');
  +-----------------+
  | fdb_get('key2') |
  +-----------------+
  | value2          |
  +-----------------+
  1 row in set (0.00 sec)
  
  mysql> select fdb_get('key2');
  +-----------------+
  | fdb_get('key2') |
  +-----------------+
  | NULL            |
  +-----------------+
  1 row in set (0.00 sec)



Examples of integration with tables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the examples above the key/value storage was used as a simple way to store and retrieve values with very simple statements that were affecting only one entry at the time.
While that is nice, the only advantage it provides over using other key/value storage ( like memcached ) is that your application doesn't need to connect to mysqld and a key/value storage.

But there is more than that! It is possible to integrate data in mysql tables with the key/value storage provided by FundaDB.
With simple SQL statements you can move data from mysql tables to FundaDB, or from FundaDB to mysql tables, or integrate them.

Example #1: insert an entry with the population of Italian cities ::

  mysql> SELECT SUM(City.Population) FROM City JOIN Country ON City.CountryCode=Country.Code WHERE Country.Name='Italy';
  +----------------------+
  | SUM(City.Population) |
  +----------------------+
  |             15087019 |
  +----------------------+
  1 row in set (0.02 sec)


  mysql> SELECT fdb_get('cities_population_Italy');
  +------------------------------------+
  | fdb_get('cities_population_Italy') |
  +------------------------------------+
  | NULL                               |
  +------------------------------------+
  1 row in set (0.00 sec)
  
  mysql> SELECT fdb_set('cities_population_Italy',CAST(SUM(City.Population) AS CHAR)) FROM City JOIN Country ON City.CountryCode=Country.Code WHERE Country.Name='Italy';
  +-----------------------------------------------------------------------+
  | fdb_set('cities_population_Italy',CAST(SUM(City.Population) AS CHAR)) |
  +-----------------------------------------------------------------------+
  |                                                                     0 |
  +-----------------------------------------------------------------------+
  1 row in set (0.01 sec)
  
  mysql> SELECT fdb_get('cities_population_Italy');
  +------------------------------------+
  | fdb_get('cities_population_Italy') |
  +------------------------------------+
  | 15087019                           |
  +------------------------------------+
  1 row in set (0.00 sec)

Example #2: insert an entry per Country with the population of their cities ::

  mysql> SELECT fdb_get('cities_population_France');
  +-------------------------------------+
  | fdb_get('cities_population_France') |
  +-------------------------------------+
  | NULL                                |
  +-------------------------------------+
  1 row in set (0.00 sec)
  
  mysql> SELECT fdb_set(CONCAT('cities_population_',Country.Name),CAST(SUM(City.Population) AS CHAR)) FROM City JOIN Country ON City.CountryCode=Country.Code GROUP BY Country.Name;
  +---------------------------------------------------------------------------------------+
  | fdb_set(CONCAT('cities_population_',Country.Name),CAST(SUM(City.Population) AS CHAR)) |
  +---------------------------------------------------------------------------------------+
  |                                                                                     0 |
  |                                                                                     0 |
  ...
  |                                                                                     0 |
  +---------------------------------------------------------------------------------------+
  232 rows in set (0.05 sec)
  
  mysql> SELECT fdb_get('cities_population_France');
  +-------------------------------------+
  | fdb_get('cities_population_France') |
  +-------------------------------------+
  | 9244494                             |
  +-------------------------------------+
  1 row in set (0.00 sec)

Example #3: find the Country with the higher population in cities ::

  mysql> SELECT Country.Name, CAST(fdb_get(CONCAT('cities_population_',Country.Name)) AS SIGNED) `fdb_population` FROM Country ORDER BY fdb_population DESC LIMIT 1;
  +-------+----------------+
  | Name  | fdb_population |
  +-------+----------------+
  | China |      175953614 |
  +-------+----------------+
  1 row in set (0.00 sec)


Notes:

* fdb_set() requires string arguments : if any of the argument is not a string you must cast

* fdb_get() returns string value : if you need the value as integer you must cast

Benchmark
~~~~~~~~~

I need to publish a proper benchmark, but here is a simple example::

  root@voyager:~# service mysql restart
  mysql stop/waiting
  mysql start/running, process 4690
  root@voyager:~# mysql -e "SELECT fdb_hash_init()" -B -N
  0
  root@voyager:~# wc -l /usr/share/dict/american-english
  99171 /usr/share/dict/american-english
  root@voyager:~# for i in `cat /usr/share/dict/american-english` ; do echo "SELECT fdb_set(\""$i\",\"$i\"");" ; done > dict1.sql
  root@voyager:~# time mysql < dict1.sql > /dev/null
  
  real    0m3.862s
  user    0m0.872s
  sys    0m0.604s
  
  root@voyager:~# mysqlslap --create-schema test -c 4 -q dict1.sql
  Benchmark
     Average number of seconds to run all queries: 6.238 seconds
     Minimum number of seconds to run all queries: 6.238 seconds
     Maximum number of seconds to run all queries: 6.238 seconds
     Number of clients running queries: 4
     Average number of queries per client: 99171
   
  root@voyager:~# for i in `cat /usr/share/dict/american-english` ; do echo "SELECT fdb_get(\""$i\"");" ; done > dict2.sql
  
  root@voyager:~# sort -R dict2.sql > dict2_rnd.sql
  
  root@voyager:~# mysqlslap --create-schema test -c 1 -q dict2_rnd.sql
  Benchmark
     Average number of seconds to run all queries: 8.482 seconds
     Minimum number of seconds to run all queries: 8.482 seconds
     Maximum number of seconds to run all queries: 8.482 seconds
     Number of clients running queries: 1
     Average number of queries per client: 99171
  
  root@voyager:~# mysqlslap --create-schema test -c 4 -q dict2_rnd.sql
  Benchmark
     Average number of seconds to run all queries: 5.873 seconds
     Minimum number of seconds to run all queries: 5.873 seconds
     Maximum number of seconds to run all queries: 5.873 seconds
     Number of clients running queries: 4
     Average number of queries per client: 99171
  
  
Result on a Intel(R) Core(TM) i7-2677M CPU @ 1.80GHz :

* 63.6k SET/s

* 66.8k GET/s

I will publish the result of mixed SET/GET workload.


Queueing system
===============

Syntax
~~~~~~

* fdb_queue_init()

  Initialize the queueing system
  
  Returns:

  - 0 : success
  - error if system already initialized

* fdb_queue_create( *'queue_name'* )

  It creates a new named queue. Queues need to be created before being used!

  Returns:

  - 0 on success

  - 1 on failure ( the queue already exist )

  Deleting a queue is currently not supported

* fdb_queue_push( *'queue_name'*, *'entry'* )

  It pushes a new entry in a named queue.

  Returns:

  - 0 on success

  - 1 on failure (the queue doesn't exist)

* fdb_queue_pop ( *'queue_name'* [ , *timeout* ] )

  It returns an entry from the named queue.

  If the named queue is empty it blocks until a new entry is inserted or timeout (microsecond) expires.

  Note that fdb_queue_pop without timeout on an empty queue blocks the connection thread until data is pushed in the queue. That means that is impossible to kill a connection where fdb_queue_pop is blocked. For this reason is recommended to *always* specify a timeout.

  Returns:

  - on success the first entry in the queue
  
  - NULL if the queue doesn't exist or the timeout expires

  Returns:

  - the value if the key is present and not expires

  *Note:* :

  - Currently there is no upper limit in the number of entries that can be stored.

  - queue names are *key sensitive*



Example with simple queries
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Examples::

  mysql>  SELECT fdb_queue_create('test1');
  +---------------------------+
  | fdb_queue_create('test1') |
  +---------------------------+
  |                         0 |
  +---------------------------+
  1 row in set (0.00 sec)
  
  mysql>  SELECT fdb_queue_push('test1','entry1');
  +----------------------------------+
  | fdb_queue_push('test1','entry1') |
  +----------------------------------+
  |                                0 |
  +----------------------------------+
  1 row in set (0.00 sec)
  
  mysql>  SELECT fdb_queue_pop('test1');
  +------------------------+
  | fdb_queue_pop('test1') |
  +------------------------+
  | entry1                 |
  +------------------------+
  1 row in set (0.00 sec)
  
  mysql>  SELECT fdb_queue_pop('test1',1000000);
  +--------------------------------+
  | fdb_queue_pop('test1',1000000) |
  +--------------------------------+
  | NULL                           |
  +--------------------------------+
  1 row in set (1.00 sec)


Example with integration with mysql tables
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Read from a table and push into the queue::

  mysql> SELECT fdb_queue_push('test1',Name) FROM Country LIMIT 3;
  +------------------------------+
  | fdb_queue_push('test1',Name) |
  +------------------------------+
  |                            0 |
  |                            0 |
  |                            0 |
  +------------------------------+
  3 rows in set (0.00 sec)
  
  mysql> SELECT fdb_queue_pop('test1',100);
  +----------------------------+
  | fdb_queue_pop('test1',100) |
  +----------------------------+
  | Aruba                      |
  +----------------------------+
  1 row in set (0.00 sec)
  
  mysql> SELECT fdb_queue_pop('test1',100);
  +----------------------------+
  | fdb_queue_pop('test1',100) |
  +----------------------------+
  | Afghanistan                |
  +----------------------------+
  1 row in set (0.00 sec)

  mysql> SELECT fdb_queue_pop('test1',100);
  +----------------------------+
  | fdb_queue_pop('test1',100) |
  +----------------------------+
  | Angola                     |
  +----------------------------+
  1 row in set (0.00 sec)



Benchmark
~~~~~~~~~

On commodity hardware ( Intel(R) Core(TM) i5-2450M CPU @ 2.50GHz ) , running 4 threads the system is able to process 68k QPS ( mixed fdb_queue_push() and fdb_queue_pop() ).


BUGS & Known Issues
===================

FundaDB relies on Glib for many of its internal.

To work properly it requires Glib >= 2.32 . If you are using an older version replace queue.c with queue_oldglib.c

In future release the configuration will be automatic.

In even future release, old versions of Glib will be considered deprecated.
